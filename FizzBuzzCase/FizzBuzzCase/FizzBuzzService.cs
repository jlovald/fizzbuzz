﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzzCase 
{
    class FizzBuzzService : IFizzBuzzService
    {
        List<(int key, string value)> keyValuePairs = new List<(int, string)>();

        /// <summary>
        /// Default constructor
        /// </summary>
        public FizzBuzzService()
        {
            keyValuePairs.Add((3, "Fizz"));
            keyValuePairs.Add((5, "Buzz"));
        }

        /// <summary>
        /// Adds a set of key-value pairs to keyValuePairs
        /// </summary>
        /// <param name="list">Set of pairs to use in FizzBuzz</param>
        public FizzBuzzService(params (int key, string value)[] list) 
        {
            foreach(var i in list)
            {
                keyValuePairs.Add(i);
            }
        }

        /// <summary>
        /// Gets a FizzBuzz from index 1 to end
        /// </summary>
        /// <param name="end">Incusive</param>
        /// <returns>List containting FizzBuzz result</returns>
        public List<string> GetFizzBuzz(int end)
        {
            return GetFizzBuzz(0, end);
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start">Exclusive</param>
        /// <param name="end">Inclusive</param>
        /// <returns>A list of FizzBuzz elements.</returns>
        public List<string> GetFizzBuzz(int start, int end)
        {
            List<string> lines = new List<string>();
            string line;
            for(var i = start + 1; i <= end; i++)
            {
                line = "";
                // For each value in keys.
                foreach(var pair in keyValuePairs)
                {
                    if(i % pair.key == 0)
                    {
                        line += pair.value; //  add string
                    }
                }
                //  If unchanged print number
                if(line == "")
                {
                    lines.Add(line + i);
                } else
                {
                    lines.Add(line);
                }
            }
            return lines;
        }
    
   
    }
}
