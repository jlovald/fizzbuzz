﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzzCase
{
    interface IFizzBuzzService
    {
        List<string> GetFizzBuzz(int start, int end);
        List<string> GetFizzBuzz(int end);

    }
}
