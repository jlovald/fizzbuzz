﻿using System;

namespace FizzBuzzCase
{
    class Program
    {
        static void Main(string[] args)
        {
            //  Given code.
            IFizzBuzzService fizzBuzzService = new FizzBuzzService();
            foreach (var result in fizzBuzzService.GetFizzBuzz(100))
            {
                Console.WriteLine(result);
            }
            Console.ReadLine();
        }
    }
}
