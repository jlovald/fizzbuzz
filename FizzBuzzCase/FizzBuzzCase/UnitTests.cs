﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;
namespace FizzBuzzCase
{
    public class UnitTests
    {
        
        private List<String> exampleOutput = new List<string>();
        
        public void Helper(string filename)
        {
            string fullPath = System.IO.Path.GetFullPath(filename);
            Console.WriteLine(fullPath);
            exampleOutput = new List<String>();
            exampleOutput = new List<string>(File.ReadAllLines(@fullPath));
        }

        [Fact]
        public void TestDefaulOutputEquality()
        {
            Helper("FizzBuzz.txt");
            IFizzBuzzService testObject = new FizzBuzzService();
            Assert.Equal(exampleOutput, testObject.GetFizzBuzz(100));
        }

        [Fact]
        public void TestParamsOutputEquality()
        {
            Helper("FizzBuzz.txt");
            FizzBuzzService testObject = new FizzBuzzService((3, "Fizz"), (5, "Buzz"));
            Assert.Equal(exampleOutput, testObject.GetFizzBuzz(100));
        }
        
        public List<string> TestInitial()
        {
            List<string> t = new List<string>();

            return t;
        }

    }
}
